<div id="footer">
    {{ date('Y') }} &copy; 
    <div class="span pull-right">
        <span class="go-top"><i class="icon-arrow-up"></i></span>
    </div>
</div>